const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const lookup = require('country-code-lookup');
const Users = require('./mock_for_L3');

function checkString(errorMessages, paramName, paramValue) {
  if (typeof (paramValue) !== 'string') {
    errorMessages.push(`Datatype of ${paramName} '${paramValue}' must be string.`);
  } else if (paramValue.length === 0) {
    errorMessages.push(`${paramName} must be longer then 0.`);
  } else if (paramValue[0].toUpperCase() !== paramValue[0]) {
    errorMessages.push(`First letter of ${paramName} '${paramValue}' must be upper case.`);
  }
}

function checkPhoneFromat(country, phone, input) {
  const region = lookup.byCountry(country).iso2;
  const number = phoneUtil.parseAndKeepRawInput(phone, region);
  const validPhone = phoneUtil.isValidNumberForRegion(number, region);
  if (!validPhone) {
    input.setCustomValidity(`Incorrect format for ${country}`);
  } else {
    input.setCustomValidity('');
  }
}

function validateObject(user) {
  const errorMessages = [];
  checkString(errorMessages, 'full_name', user.full_name);
  checkString(errorMessages, 'note', user.note);
  checkString(errorMessages, 'state', user.state);
  checkString(errorMessages, 'city', user.city);
  checkString(errorMessages, 'country', user.country);

  if (typeof (user.gender) !== 'string') {
    errorMessages.push(`Datatype of gender '${user.gender}' must be string.`);
  } else if (user.gender.length === 0) {
    errorMessages.push('Gender must be longer then 0.');
  }

  if (typeof (user.age) !== 'number') {
    errorMessages.push(`Datatype of age '${user.age}' must be number.`);
  } else if (user.age < 12) {
    errorMessages.push(`Age '${user.age}' is incorrect, teacher must be older then 12 years old.`);
  }

  if (typeof (user.phone) === 'string') {
    const region = lookup.byCountry(user.country).iso2;
    const number = phoneUtil.parseAndKeepRawInput(user.phone, region);
    const validPhone = phoneUtil.isValidNumberForRegion(number, region);
    if (!validPhone) {
      errorMessages.push(`Phone '${user.phone}' is incorrect for ${user.country}.`);
    }
  }

  if (typeof (user.email) !== 'string') {
    errorMessages.push(`Datatype of email '${user.email}' must be string.`);
  } else if (!user.email.includes('@')) {
    errorMessages.push(`Email '${user.email}' must include symbol '@'.`);
  } else if (user.email.length < 3) {
    errorMessages.push(`Email '${user.email}' must contain at least 3 symbols.`);
  } else if (user.email[0] === '@') {
    errorMessages.push(`Email '${user.email}' can't start from symbol '@'.`);
  } else if (user.email[user.email.length - 1] === '@') {
    errorMessages.push(`Email '${user.email}' can't end on symbol '@'.`);
  }

  return errorMessages;
}

function createFormattedArray() {
  const courses = [null, 'Mathimatics', 'Chemistry', 'Music', 'Physics'];
  const usersArray = [];
  Users.randomUserMock.forEach((userItem) => {
    const item = {};
    item.gender = userItem.gender;
    item.title = userItem.name.title;
    item.full_name = userItem.name.first.concat(' ', userItem.name.last);
    item.city = userItem.location.city;
    item.state = userItem.location.state;
    item.country = userItem.location.country;
    item.postcode = userItem.location.postcode;
    item.coordinates = userItem.location.coordinates;
    item.timezone = userItem.location.timezone;
    item.email = userItem.email;
    item.b_day = userItem.dob.date;
    item.age = userItem.dob.age;
    item.phone = userItem.phone;
    item.picture_large = userItem.picture.large;
    item.picture_thumbnail = userItem.picture.thumbnail;
    item.id = Math.random().toString(36).substr(2, 15);
    const favoriteNumber = Math.floor(Math.random() * Math.floor(2));
    if (favoriteNumber === 1) {
      item.favorite = true;
    } else {
      item.favorite = false;
    }
    const courseIndex = Math.floor(Math.random() * Math.floor(courses.length));
    item.course = courses[courseIndex];
    item.bg_color = '#'.concat(Math.floor(Math.random() * 16777215).toString(16));
    item.note = 'Note will be there';
    let added = true;
    usersArray.forEach((addedItem) => {
      if (addedItem.full_name === item.full_name
        && addedItem.city === item.city && addedItem.b_day === item.b_day) {
        added = false;
      }
    });
    if (added) {
      const errorMessages = validateObject(item);
      if (!errorMessages.length) {
        usersArray.push(item);
      } else {
        console.log(errorMessages);
      }
    }
  });
  Users.additionalUsers.forEach((userItem) => {
    let added = true;
    usersArray.forEach((addedItem) => {
      if (addedItem.full_name === userItem.full_name
        && addedItem.city === userItem.city && addedItem.b_day === userItem.b_day) {
        added = false;
      }
    });
    if (added) {
      const errorMessages = validateObject(userItem);
      if (!errorMessages.length) {
        usersArray.push(userItem);
      } else {
        console.log(errorMessages);
      }
    }
  });
  return usersArray;
}

function filterArray(usersArray, country, gender, favorite, minAge, maxAge) {
  const filteredArray = [];
  usersArray.forEach((user) => {
    let added = true;
    if (country !== 'All countries' && user.country !== country) {
      added = false;
    }
    if (gender === 'male' || gender === 'female') {
      if (user.gender !== gender) {
        added = false;
      }
    }
    if (favorite === true && user.favorite === false) {
      added = false;
    }
    if (parseInt(user.age, 10) < parseInt(minAge, 10)
    || parseInt(user.age, 10) > parseInt(maxAge, 10)) {
      added = false;
    }
    if (added) {
      filteredArray.push(user);
    }
  });
  return filteredArray;
}

function sortArray(usersArray, param, increasing) {
  const sortedArray = usersArray.slice();
  if (param === 'country' || param === 'full_name'
  || param === 'b_day' || param === 'age') {
    if (increasing) {
      sortedArray.sort((a, b) => {
        if (a[param] < b[param]) {
          return -1;
        }
        if (a[param] > b[param]) {
          return 1;
        }
        return 0;
      });
    } else {
      sortedArray.sort((a, b) => {
        if (a[param] > b[param]) {
          return -1;
        }
        if (a[param] < b[param]) {
          return 1;
        }
        return 0;
      });
    }
  } else {
    console.log(`Incorrect parameter for sorting ${param}`);
  }
  return sortedArray;
}

function searchByParameter(usersArray, param) {
  const foundUsers = [];
  const paramStr = param.toString();
  usersArray.forEach((userItem) => {
    if (userItem.full_name.toUpperCase().includes(paramStr.toUpperCase())
     || userItem.age.toString() === paramStr) {
      foundUsers.push(userItem);
    } else if (typeof (userItem.note) === 'string') {
      if (userItem.note.toUpperCase().includes(paramStr.toUpperCase())) {
        foundUsers.push(userItem);
      }
    }
  });
  return foundUsers;
}

function getPercent(usersArray, param) {
  const findedUsers = searchByParameter(usersArray, param);
  const percent = (findedUsers.length * 100) / usersArray.length;
  return Math.round(percent);
}

module.exports = {
  createFormattedArray,
  filterArray,
  sortArray,
  searchByParameter,
  getPercent,
  checkPhoneFromat,
};
