const teacherArray = require('./teacherArray');
require('../css/app.css');

const elements = {
  target: null,
  teacher: null,
  teachers: teacherArray.createFormattedArray(),
  soreted: null,
};

elements.currentTeachers = elements.teachers;
elements.countires = [];

function setPopupCountiers() {
  elements.teachers.forEach((teacher) => {
    elements.countires.push(teacher.country);
  });
  elements.countires = elements.countires
    .filter((value, index, self) => self.indexOf(value) === index);
  const selectCountry = document.querySelector('.popup-add-teacher form select');
  elements.countires.forEach((country) => {
    const option = document.createElement('option');
    option.textContent = country;
    selectCountry.appendChild(option);
  });
}

function setFilterCountries() {
  const selectCountry = document.querySelector('.top-teachers select');
  elements.countires.forEach((country) => {
    const option = document.createElement('option');
    option.textContent = country;
    selectCountry.appendChild(option);
  });
}

function addTablePagination(page, teachers) {
  const from = (page - 1) * 10;
  const to = from + 10;
  const list = [];
  let i = from;
  while (i < to && i < teachers.length) {
    list.push(teachers[i]);
    i += 1;
  }
  return list;
}

function addImage(teacher) {
  const teacherImg = document.querySelector('.popup-teacher-card figure img');
  teacherImg.src = teacher.picture_large;
  const initials = document.querySelector('.popup-teacher-card figure .popup-initials');
  teacherImg.onload = () => {
    teacherImg.style.display = 'block';
    initials.className += ' hidden-initials';
  };
  teacherImg.onerror = () => {
    teacherImg.style.display = 'none';
    initials.className = 'popup-initials';
    const p = document.querySelector('.popup-teacher-card figure .popup-initials p');
    let lastNameIndex;
    for (let i = 0; i < teacher.full_name.length; i += 1) {
      if (teacher.full_name[i] === ' ') {
        lastNameIndex = i + 1;
        break;
      }
    }
    p.textContent = teacher.full_name[0].concat('.', teacher.full_name[lastNameIndex]);
  };
}

function fillPopup(target, teacher) {
  const teacherInfoStar = document.querySelector('.popup-teacher-card .teacher-info-star');
  if (teacher.favorite === true) {
    teacherInfoStar.textContent = '\u2605';
  } else {
    teacherInfoStar.textContent = '\u2606';
  }
  elements.target = target;
  elements.teacher = teacher;
  const popupFullname = document.querySelector('.popup-teacher-card .teacher-info h1');
  popupFullname.textContent = teacher.full_name;
  const location = document.querySelector('.popup-teacher-card .teacher-info .location');
  location.textContent = teacher.city.concat(',', teacher.country);
  const ageGender = document.querySelector('.popup-teacher-card .teacher-info .age-gender');
  ageGender.textContent = teacher.age;
  if (teacher.gender === 'male') {
    ageGender.textContent += ',M';
  } else {
    ageGender.textContent += ',F';
  }
  const email = document.querySelector('.popup-teacher-card .teacher-info .email');
  email.textContent = teacher.email;
  const phone = document.querySelector('.popup-teacher-card .teacher-info .phone');
  phone.textContent = teacher.phone;
  addImage(teacher);
  const comment = document.querySelector('.popup-teacher-card .comment p');
  const commentClass = document.querySelector('.popup-teacher-card .comment');
  comment.textContent = teacher.note;
  if (comment.textContent.length === 0) {
    commentClass.style.display = 'none';
  } else {
    commentClass.style.display = 'block';
  }
}

function fillCard(teacher) {
  const item = {};
  item.teacherCard = document.createElement('span');
  item.teacherCard.className += 'teacher-card';
  item.popup = document.createElement('a');
  item.popup.href = '#popup-teacher-card';
  item.popup.addEventListener('click', (event) => {
    fillPopup(event.currentTarget, teacher);
  });
  item.figure = document.createElement('figure');
  item.country = document.createElement('p');
  item.country.textContent = teacher.country;
  return item;
}

function fillCardFigure(teacher, figure) {
  const figcaption = document.createElement('figcaption');
  const fullName = teacher.full_name.split(' ');
  figcaption.appendChild(document.createTextNode(fullName[0]));
  figcaption.appendChild(document.createElement('br'));
  figcaption.appendChild(document.createTextNode(fullName[1]));
  const img = document.createElement('img');
  img.src = teacher.picture_large;
  img.onload = () => {
    figure.appendChild(img);
    figure.appendChild(figcaption);
  };
  img.onerror = () => {
    const initials = document.createElement('div');
    initials.className += 'initials';
    const p = document.createElement('p');
    p.textContent = p.textContent.concat(fullName[0][0], '.', fullName[1][0]);
    initials.appendChild(p);
    figure.appendChild(initials);
    figure.appendChild(figcaption);
  };
}

function createTopTeacherCards(teachers) {
  const fragment = document.createDocumentFragment();
  teachers.forEach((teacher) => {
    const element = fillCard(teacher);
    const star = document.createElement('span');
    star.className += 'star';
    star.textContent = '\u2605';
    if (teacher.favorite === false) {
      star.className += ' hidden-star';
    }
    element.popup.appendChild(star);
    element.teacherCard.appendChild(element.popup);
    fillCardFigure(teacher, element.figure);
    element.popup.append(element.figure, element.country);
    fragment.appendChild(element.teacherCard);
  });
  return fragment;
}

function createFavoriteTeacherCard(teachers) {
  const fragment = document.createDocumentFragment();
  teachers.forEach((teacher) => {
    if (!teacher.favorite) {
      return;
    }
    const element = fillCard(teacher);
    element.teacherCard.appendChild(element.popup);
    fillCardFigure(teacher, element.figure);
    element.popup.append(element.figure, element.country);
    fragment.appendChild(element.teacherCard);
  });
  return fragment;
}

function createTableRows(teachers) {
  const fragment = document.createDocumentFragment();
  const currentPage = document.querySelector('.statistics nav .current').textContent;
  const list = addTablePagination(currentPage, teachers);
  list.forEach((teacher) => {
    const row = document.createElement('tr');
    const fullName = document.createElement('td');
    fullName.textContent = teacher.full_name;
    const age = document.createElement('td');
    age.textContent = teacher.age;
    const gender = document.createElement('td');
    gender.textContent = teacher.gender;
    const country = document.createElement('td');
    country.textContent = teacher.country;
    row.append(fullName, age, gender, country);
    fragment.appendChild(row);
  });
  return fragment;
}

function fillTopTeachers() {
  const topTeachers = document.querySelector('.top-teachers .container');
  const topTeacherCards = createTopTeacherCards(elements.currentTeachers);
  topTeachers.appendChild(topTeacherCards);
}

function fillStatistics() {
  const statistics = document.querySelector('.statistics table');
  const statisticsTable = createTableRows(elements.currentTeachers);
  statistics.appendChild(statisticsTable);
}

function fillFavorites() {
  const favorites = document.querySelector('.favorites .container');
  const favoritesCards = createFavoriteTeacherCard(elements.currentTeachers);
  favorites.appendChild(favoritesCards);
}

function reloadTeachers() {
  const topTeachersList = document.querySelector('.top-teachers .container');
  while (topTeachersList.lastElementChild) {
    topTeachersList.removeChild(topTeachersList.lastElementChild);
  }
  fillTopTeachers();
  const favoritesList = document.querySelector('.favorites .container');
  while (favoritesList.lastElementChild) {
    favoritesList.removeChild(favoritesList.lastElementChild);
  }
  fillFavorites();
}

function navigatePage(event) {
  const currentPage = event.currentTarget;
  const nav = document.querySelector('.statistics nav');
  nav.childNodes.forEach((node) => {
    const page = node;
    page.className = '';
  });
  currentPage.className = 'current';
  const rows = document.querySelector('.statistics table');
  let i = rows.childElementCount - 1;
  while (i > 1) {
    rows.removeChild(rows.lastElementChild);
    i -= 1;
  }
  fillStatistics();
}

function addTablePages(teachers) {
  let pages = Math.ceil(teachers.length / 10);
  if (pages === 0) {
    pages = 1;
  }
  const nav = document.querySelector('.statistics nav');
  while (nav.lastElementChild) {
    nav.removeChild(nav.lastElementChild);
  }
  let i = 1;
  while (i <= pages) {
    const page = document.createElement('p');
    page.textContent = i;
    page.addEventListener('click', navigatePage);
    nav.appendChild(page);
    i += 1;
  }
  nav.firstElementChild.className = 'current';
}

function reloadPageData() {
  const firstPage = document.querySelector('.statistics nav p');
  const event = new Event('click');
  firstPage.dispatchEvent(event);
  addTablePages(elements.currentTeachers);
  reloadTeachers();
}

const popupStar = document.querySelector('.popup-teacher-card .teacher-info-star');
popupStar.addEventListener('click', () => {
  if (popupStar.textContent === '\u2606') {
    popupStar.textContent = '\u2605';
  } else {
    popupStar.textContent = '\u2606';
  }
});

const popupClose = document.querySelector('.popup-teacher-card .close');
popupClose.addEventListener('click', () => {
  if ((popupStar.textContent === '\u2605' && elements.teacher.favorite)
  || (popupStar.textContent === '\u2606' && !elements.teacher.favorite)) {
    return;
  }
  if (popupStar.textContent === '\u2605') {
    elements.teacher.favorite = true;
    elements.target.childNodes[0].className = 'star';
  } else {
    elements.teacher.favorite = false;
    elements.target.childNodes[0].className = 'star hidden-star';
  }
  reloadTeachers();
});

const tableHeaders = document.querySelectorAll('.statistics table th');
tableHeaders[0].addEventListener('click', () => {
  if (elements.soreted !== 'full_name') {
    elements.currentTeachers = teacherArray.sortArray(elements.currentTeachers, 'full_name', true);
    elements.soreted = 'full_name';
  } else {
    elements.currentTeachers = teacherArray.sortArray(elements.currentTeachers, 'full_name', false);
    elements.soreted = null;
  }
  reloadPageData();
});

tableHeaders[1].addEventListener('click', () => {
  if (elements.soreted !== 'b_day') {
    elements.currentTeachers = teacherArray.sortArray(elements.currentTeachers, 'b_day', true);
    elements.soreted = 'b_day';
  } else {
    elements.currentTeachers = teacherArray.sortArray(elements.currentTeachers, 'b_day', false);
    elements.soreted = null;
  }
  reloadPageData();
});

tableHeaders[3].addEventListener('click', () => {
  if (elements.soreted !== 'country') {
    elements.currentTeachers = teacherArray.sortArray(elements.currentTeachers, 'country', true);
    elements.soreted = 'country';
  } else {
    elements.currentTeachers = teacherArray.sortArray(elements.currentTeachers, 'country', false);
    elements.soreted = null;
  }
  reloadPageData();
});

function setFilters() {
  const country = document.querySelector('.top-teachers select').value;
  let gender;
  if (document.querySelectorAll('.top-teachers .gender input')[1].checked) {
    gender = 'female';
  } else if (document.querySelectorAll('.top-teachers .gender input')[2].checked) {
    gender = 'male';
  } else {
    gender = '';
  }
  const minAge = document.querySelector('.top-teachers .min-age').value;
  const maxAge = document.querySelector('.top-teachers .max-age').value;
  let favorite = false;
  if (document.querySelector('.top-teachers .checkbox-favorites').checked) {
    favorite = true;
  }
  elements.currentTeachers = teacherArray
    .filterArray(elements.currentTeachers, country, gender, favorite, minAge, maxAge);
}

const btnSearch = document.querySelector('.header .search form');
btnSearch.addEventListener('submit', (e) => {
  e.preventDefault();
  e.stopImmediatePropagation();
  const searchParam = document.querySelector('.header .search .txt-search').value;
  elements.currentTeachers = teacherArray
    .searchByParameter(elements.teachers, searchParam);
  setFilters();
  reloadPageData();
});

function getAge(dateString) {
  const today = new Date();
  const birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age -= 1;
  }
  return age;
}

const addTeacherForm = document.querySelector('.popup-add-teacher form');
function addTeacher(e) {
  e.preventDefault();
  e.stopImmediatePropagation();
  const teacher = {};
  if (document.getElementsByName('gender')[0].checked) {
    teacher.gender = 'female';
  } else {
    teacher.gender = 'male';
  }
  if (teacher.gender === 'male') {
    teacher.title = 'Mr';
  } else {
    teacher.title = 'Ms';
  }
  teacher.full_name = document.getElementsByName('name')[0].value;
  teacher.city = document.getElementsByName('city')[0].value;
  teacher.state = '';
  teacher.country = document.querySelector('.popup-add-teacher form select').value;
  teacher.postcode = null;
  teacher.coordinates = null;
  teacher.timezone = null;
  teacher.email = document.getElementsByName('email')[0].value;
  teacher.b_day = document.getElementsByName('date_of_birth')[0].value;
  teacher.b_day = new Date(teacher.b_day);
  teacher.b_day = teacher.b_day.toISOString();
  teacher.age = getAge(teacher.b_day);
  teacher.phone = document.getElementsByName('phone')[0].value;
  teacher.picture_large = '';
  teacher.picture_thumbnail = '';
  teacher.id = Math.random().toString(36).substr(2, 15);
  teacher.favorite = false;
  teacher.course = null;
  teacher.bg_color = document.getElementsByName('back_color')[0].value;
  teacher.note = '';
  elements.teachers.push(teacher);
  elements.currentTeachers = elements.teachers;
  reloadPageData();
  const searchInput = document.querySelector('.header .search .txt-search');
  searchInput.value = '';
  document.location.href = '#';
}

addTeacherForm.addEventListener('submit', addTeacher);

const phoneInput = document.getElementsByName('phone')[0];
phoneInput.addEventListener('input', () => {
  const country = document.querySelector('.popup-add-teacher form select').value;
  teacherArray.checkPhoneFromat(country, phoneInput.value, phoneInput);
});

const selectCountry = document.querySelector('.popup-add-teacher form select');
selectCountry.addEventListener('change', () => {
  const event = new Event('input');
  phoneInput.dispatchEvent(event);
});

function filterTeachers() {
  const event = new Event('submit');
  btnSearch.dispatchEvent(event);
  setFilters();
  reloadPageData();
}

const filterCountries = document.querySelector('.top-teachers select');
filterCountries.addEventListener('change', filterTeachers);

const filterGenderRadioButton = document.querySelectorAll('.top-teachers .gender input');
filterGenderRadioButton[0].addEventListener('click', filterTeachers);
filterGenderRadioButton[1].addEventListener('click', filterTeachers);
filterGenderRadioButton[2].addEventListener('click', filterTeachers);

const filterFavorites = document.querySelector('.top-teachers .checkbox-favorites');
filterFavorites.addEventListener('change', filterTeachers);

const filterMinAge = document.querySelector('.top-teachers .min-age');
const filterMaxAge = document.querySelector('.top-teachers .max-age');
filterMinAge.addEventListener('change', () => {
  if (filterMinAge.value === '') {
    filterMinAge.value = '16';
  }
  filterMaxAge.min = filterMinAge.value;
  filterTeachers();
});
filterMaxAge.addEventListener('change', () => {
  if (filterMaxAge.value === '') {
    filterMaxAge.value = '100';
  }
  filterMinAge.max = filterMaxAge.value;
  filterTeachers();
});

function sideScroll(element, direction, speed, distance, step) {
  let scrollAmount = 0;
  const slideTimer = setInterval(() => {
    if (direction === 'left') {
      // eslint-disable-next-line no-param-reassign
      element.scrollLeft -= step;
    } else {
      // eslint-disable-next-line no-param-reassign
      element.scrollLeft += step;
    }
    scrollAmount += step;
    if (scrollAmount >= distance) {
      window.clearInterval(slideTimer);
    }
  }, speed);
}

const rightArrow = document.querySelector('.right-arrow');
rightArrow.addEventListener('click', () => {
  const container = document.querySelector('.favorites .container');
  sideScroll(container, 'right', 10, 230, 10);
});

const leftArrow = document.querySelector('.left-arrow');
leftArrow.addEventListener('click', () => {
  const container = document.querySelector('.favorites .container');
  sideScroll(container, 'left', 10, 230, 10);
});

setPopupCountiers();
setFilterCountries();

fillTopTeachers();

addTablePages(elements.currentTeachers);
fillStatistics();

fillFavorites();
